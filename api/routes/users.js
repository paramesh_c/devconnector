const express = require('express');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const router = express.Router();

const User = require('../../models/User');

const { check, validationResult } = require('express-validator');

// @route POST api/users
// @desc Test Route
// @access Public
router.post(
  '/',
  [
    check('name', 'Name is required.')
      .not()
      .isEmpty(),
    // username must be an email
    check('email', 'Please include a valid email.').isEmail(),
    // password must be at least 5 chars long
    check('password', 'Please a password with min 6 characters.').isLength({
      min: 6
    })
  ],
  async (req, res) => {
    var errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty()) {
      return res.status(400).json({ error: errors.array() });
    }

    const { name, email, password } = req.body;
    try {
      let user = await User.findOne({ email });
      if (user) {
        res.status(400).json({ errors: ['User Already Exists'] });
      }
      const avatar = gravatar.url(email, {
        s: 200,
        r: 'pg',
        d: 'mm'
      });

      user = new User({
        name,
        email,
        avatar,
        password
      });

      const saltedKey = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, saltedKey);

      await user.save();

      let payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get('jwtSecretToken'),
        { expiresIn: '1d' },
        (err, token) => {
          if (err) throw err;
          res.json({ token: token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Internal Server Error');
    }
  }
);

module.exports = router;
