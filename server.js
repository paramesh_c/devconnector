const express = require('express');

const dbConnection = require('./config/dbconnection');

const app = express();

const PORT = process.env.PORT || 5000;

//Connect to Mongo DB
dbConnection();

app.use(express.json({ extended: false }));

app.listen(PORT, () => {
  console.log('Server started on port' + PORT);
});

app.get('/', (req, res) => {
  res.send('Welcome to Dev Connector');
});

app.use('/api/users', require('./api/routes/users'));
app.use('/api/profile', require('./api/routes/profile'));
app.use('/api/posts', require('./api/routes/posts'));
app.use('/api/auth', require('./api/routes/auth'));
